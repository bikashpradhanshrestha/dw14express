import { Users } from "../schema/model.js";
import { sendEmail } from "../utils/sendMail.js";
import bcrypt from "bcrypt";

export let createUser = async (req, res, next) => {
  let data = req.body;
  //save data to Users table
  try {
    data.password= await bcrypt.hash(data.password,10);
    let result = await Users.create(data);
    await sendEmail({
      from:"B.P Shrestha<bikashpradhanshrestha@gmail.com>",
      to:[data.email],
      subject:"user registration",
      html:`<p>Dear${data.fullName},hello world </p>`
    });
    res.status(200).json({
      success: true,
      message: "user created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllUser = async (req, res, next) => {
  let {sort,select,limit,page,...myquery}=req.query;
  try {
    let result = await Users.find(myquery).sort(sort).select(select).limit(limit).skip((page-1)*limit);
     
    // result = await Users.find({fullName:"deepak"});
    // result = await Users.find({fullName:"deepak"}).select("-fullName-password email");
    res.status(200).json({
      success: true,
      message: "user read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificUser = async (req, res, next) => {
  try {
    let result = await Users.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "user read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateUser = async (req, res, next) => {
  try {
    let result = await Users.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "User updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUser = async (req, res, next) => {
  try {
    let result = await Users.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "User deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};


export const loginUser = async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  try {
    let user = await Users.findOne({ email: email });//{}or null

    if (user === null) {
      res.status(401).json({ success: false, message: "Invalid credention" });
    } else {
      let dbPassword = user.password;

      let isValidPassword = await bcrypt.compare(password, dbPassword);
      if (isValidPassword) {
        res.status(200).json({ success: true, message: "login successfully." });
      } else {
        res
          .status(401)
          .json({ success: "false", message: "Invalid credential" });
      }
    }
  } catch (error) {
    res.status(401).json({
      success: false,
      message: "Invalid credential.",
    });
  }

  //passsword and dbPassword match  send success response
};