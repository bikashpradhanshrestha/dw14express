// import { WebWebUsers } from "../schema/model.js";
// import { sendEmail } from "../utils/sendMail.js";
// import bcrypt from "bcrypt";

import { WebUsers } from "../schema/model.js";
import { sendEmail } from "../utils/sendMail.js";
import bcrypt from "bcrypt";

export let createWebUser = async (req, res, next) => {
  let data = req.body;
  //save data to WebUsers table
  try {
    data.password= await bcrypt.hash(data.password,10);
    let result = await WebUsers.create(data);
    await sendEmail({
      from:"B.P Shrestha<bikashpradhanshrestha@gmail.com>",
      to:[data.email],
      subject:"webUser registration",
      html:`<p>Dear${data.fullName},hello world </p>`
    });
    res.status(200).json({
      success: true,
      message: "webUser created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllWebUser = async (req, res, next) => {
  let {sort,select,limit,page,...myquery}=req.query;
  try {
    let result = await WebUsers.find(myquery).sort(sort).select(select).limit(limit).skip((page-1)*limit);
     
    // result = await WebUsers.find({fullName:"deepak"});
    // result = await WebUsers.find({fullName:"deepak"}).select("-fullName-password email");
    res.status(200).json({
      success: true,
      message: "webUser read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificWebUser = async (req, res, next) => {
  try {
    let result = await WebUsers.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "webUser read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateWebUser = async (req, res, next) => {
  try {
    let result = await WebUsers.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "WebUser updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteWebUser = async (req, res, next) => {
  try {
    let result = await WebUsers.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "WebUser deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};


export const loginWebUser = async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  try {
    let webUser = await WebUsers.findOne({ email: email });//{}or null

    if (webUser === null) {
      res.status(401).json({ success: false, message: "Invalid credention" });
    } else {
      let dbPassword = webUser.password;

      let isValidPassword = await bcrypt.compare(password, dbPassword);
      if (isValidPassword) {
        res.status(200).json({ success: true, message: "login successfully." });
      } else {
        res
          .status(401)
          .json({ success: "false", message: "Invalid credential" });
      }
    }
  } catch (error) {
    res.status(401).json({
      success: false,
      message: "Invalid credential.",
    });
  }

  //passsword and dbPassword match  send success response
};