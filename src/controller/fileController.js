export const handleSingleFile=(req,res,next)=>{
    // console.log(req.body);
    console.log(req.file)
    let link = `http://localhost:8000/${req.file.filename}`;
    res.json({
        success:true,
        message:"file uploaded successfully.",
        result:link,
    });
};
export const handleMultipleFile=(req,res,next)=>{
    console.log(req.files);
    let links =req.files.map((item,i)=>{
        return`http://localhost:8000/${item.filename}`}
    );
    res.json({
        success:true,
        message:"file uploaded successfully",
        result:links
    })
};