//import {Admins } from "../schema/model.js";

import { Admins } from "../schema/model.js";

export let createAdmin = async (req, res, next) => {
  let data = req.body;
  //save data toAdmins table
  try {
    let result = await Admins.create(data);
    res.status(200).json({
      success: true,
      message: "admin created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllAdmin = async (req, res, next) => {
  let {sort,select,limit,page,...myquery}=req.query;
  try {
    let result = awaitAdmins.find(myquery).sort(sort).select(select).limit(limit).skip((page-1)*limit);
     
    // result = awaitAdmins.find({fullName:"deepak"});
    // result = awaitAdmins.find({fullName:"deepak"}).select("-fullName-password email");
    res.status(200).json({
      success: true,
      message: "admin read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificAdmin = async (req, res, next) => {
  try {
    let result = awaitAdmins.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "admin read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateAdmin = async (req, res, next) => {
  try {
    let result = awaitAdmins.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "Admin updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteAdmin = async (req, res, next) => {
  try {
    let result = awaitAdmins.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "Admin deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
