//import { Colleges } from "../schema/model.js";

import envVar from "../constant.js";
import { Colleges } from "../schema/model.js";

export let createCollege = async (req, res, next) => {
  let data = req.body;
  let link = `${envVar.serverUrl}/${req.file.filename}`;
  data.collegeImage = link;
  //save data to Colleges table
  try {
    let result = await Colleges.create(data);
    res.status(200).json({
      success: true,
      message: "college created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllCollege = async (req, res, next) => {
  try {
    let result = await Colleges.find({});
    res.status(200).json({
      success: true,
      message: "college read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "college read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "College updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "College deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
