import { Schema } from "mongoose";
//create
// manipulation
  

//validation

let productSchema = Schema({
  name: {
    type: String,
    // lowercase: true,
    // uppercase: true,
    // trim: true,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  isAvailable: {
    type: Boolean,
    default: true,
    required: true,
  },
});

export default productSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of product
Schema
model
 */
