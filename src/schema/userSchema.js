/* 
fullName, 
password,
email,
gender,
address
*/

import { Schema } from "mongoose";

let userSchema = Schema({
  fullName: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    unique:true,
  },
  gender: {
    type: String,
    required: false,
    lowercase: true,
    trim: true,
  },
  address: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
});

export default userSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of product
Schema
model


productId
description
userId



 */
