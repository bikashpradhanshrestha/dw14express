import { Schema } from "mongoose";
//create
// manipulation

//validation

let adminSchema = Schema({
  name: {
    type: String,
    required: [true, "name filed is required"],
    lowercase: true,
    trim: true,
    minLength: [3, "name must be at least 3 characters  long"],
    maxLength: [30, "name must be 30 characters"],
    validate: (value) => {
      let isvalid = /^[a-zA-Z]+$/.test(value);
      if (!isvalid) {
        let error = new error("name must be only alphabet");
        throw error;
      }
    },
  },
  password: {
    type: String,
    required: true,
    validate: (value) => {
      let isvalid =
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+{}|:"<>?`\-=[\];',.\/]).{8,20}$/.test(
          value
        );
      if (!isvalid) {
        let error = new Error(
          "regex for password with min 8 character max 20 character must have atleast one number must have atleast one smallercase and uppercase and one symbol"
        );
        throw error;
      }
    }
  },
    phoneNumber: {
      type: Number,
      required: true,
      lowercase: true,
      trim: true,
      validate: (value) => {
        let sametrphoneNumber = String(value);
        let strphoneNumberLength = strphoneNumber.Length;
        if (strphoneNumberLength !== 10) {
          let error = new error("phone number must be exact 10 digits");
          throw error;
        }
      },
    },
    roll: {
      type: Number,
      required: true,
      trim: true,
      min: [20, "roll must be greather then 20"],
      max: [30, "roll must be less then 30"],
    },
    isMarried: {
      type: Boolean,
      required: true,
      trim: true,
    },
    spouseName: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      trim: true,
    },
    gender: {
      type: String,
      required: true,
      trim: true,
      validate: (value) => {
        if (value === "male" || value === "female" || value === "other") {
        } else {
          let error = new Error("gender must be either male, female or other");
          throw error;
        }
      },
    },
    dob: {
      type: Date,
      required: true,
      trim: true,
    },
    location: {
      country: {
        type: String,
      },
      exactLocation: {
        type: String,
      },
    },
    favTeacher: [
      {
        type: String,
      },
    ],
    favSubject: [
      {
        bookName: {
          type: String,
        },
        bookAuthor: {
          type: String,
        },
      },
    ],
  },
);

export default adminSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of admin
Schema
model
 */
//inbuild
