/* 
fullName, 
password,
email,
gender,
address
*/

import { Schema } from "mongoose";

let webUserSchema = Schema({
  fullName: {
    type: String,
    required: [true,"name field is required"],
    lowercase: true,
    trim: true,
  },
  email: {
    type: String,
    required: [true,"email field is requird"],
  },
    
  password: {
    type : String,
    required:[ true,"password field is required"],
    lowercase: true,
    trim: true,
    unique:true,
  },
  dob:{
    type:String,
    require:[ true,"dob field is required"],
  },
  gender: {
    type: String,
    required:[ true,"gender field is required"],
    lowercase: true,
    trim: true,
  },
  role: {
    type: String,
    required:[ true,"role field is required"],
  
    lowercase: true,
    trim: true,
  },
  isVerifiedEmail:{
    type: Boolean,
    required:[ true,"isVerifiedEmail field is required"],
  
  },
  
},{timestapms: true});

export default webUserSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of product
Schema
model


productId
description
webwebUserId



 */
