/* 
fullName, 
password,
email,
gender,
address
*/

import { Schema } from "mongoose";

//import { Schema } from "mongoose";

let collegeSchema = Schema({
  name: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
  address: {
    type: String,
    required: true,
  },
  
  collegeImage: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
});

export default collegeSchema;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required
*/

/* 
define structure of product
Schema
model


productId
description
userId



 */
