import { Router } from "express";

let firstRouter = Router();
const getController1=(req,res,next)=>{
  console.log("hello world");
};
const getController2=(a,b)=>{
  return(req,res,next)=>{
    console.log(a,b);
  };
};

firstRouter
  .route("/") //localhost:8000/name
  .post(
    (req, res, next) => {
      console.log("i am middleware 1");
      let error = new Error("error 1");
      next(error);
    },
    (err, req, res, next) => {
      console.log("i am 1 error middleware");

      next();
    },
    (req, res, next) => {
      console.log("i am middleware 2");

      next();
    },
    (req, res, next) => {
      console.log("i am middleware 3");
    }
  )
  //.get(getController1)
  .get(getController2(1,2))//use this if you want to pass this value
  .patch(() => {
    console.log("home patch");
  })
  .delete(() => {
    console.log("home delete");
  });

firstRouter
  .route("/name") //localhost:8000/name/name
  .post(() => {
    console.log("name post");
  })
  .get(() => {
    console.log("name get");
  })
  .patch(() => {
    console.log("name patch");
  })
  .delete(() => {
    console.log("name delete");
  });

export default firstRouter;

/* 
2 types of middleware
normal middleware
(req,res,next)=>{}
to trigger next middleware we have call next()



error middleware
(err, req,res,next)=>{}
to trigger next middleware we have to call next(err)



*/
