import { Router } from "express";

import {
  createProduct,
  deleteProduct,
  readAllProduct,
  readSpecificProduct,
  updateProduct,
} from "../controller/productController.js";

let productRouter = Router();

productRouter
  .route("/") //localhost:8000/product
  .post(createProduct)
  .get(readAllProduct);

productRouter
  .route("/:id") //localhost:8000/product/any
  .get(readSpecificProduct)
  .patch(updateProduct)
  .delete(deleteProduct);

export default productRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create product
get all product

get specific product


Product.create(req.body)
Product.find({})
Product.findById(req.params.id)
Product.findByIdAndUpdate(req.params.id,req.body)
Product.findByIdAndDelete(req.params.id)

*/
