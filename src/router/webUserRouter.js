/// import { Router } from "express";
// import {
//   createWebUser,
//   deleteWebUser,
//   loginWebUser,
//   readAllWebUser,
//   readSpecificWebUser,
//   updateWebUser,
// } from "../controller/webUserController.js";

import { Router } from "express";
import { createWebUser, deleteWebUser, readAllWebUser, readSpecificWebUser, updateWebUser } from "../controller/webUserController.js";

let webUserRouter = Router();
//localhost:8000/webUser =>give all webUser
//localhost:8000/webUser?fullName

webUserRouter
  .route("/") //localhost:8000/webUser
  .post(createWebUser)
  .get(readAllWebUser);

webUserRouter.route("/login").post(loginWebUser);

webUserRouter
  .route("/:id") //localhost:8000/webUser/any
  .get(readSpecificWebUser)
  .patch(updateWebUser)
  .delete(deleteWebUser);


export default webUserRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create webUser
get all webUser

get specific webUser


WebUser.create(req.body)
WebUser.find({})
WebUser.findById(req.params.id)
WebUser.findByIdAndUpdate(req.params.id,req.body)
WebUser.findByIdAndDelete(req.params.id)

*/
// always place dynamic route at last