// import { Router } from "express";
// import {
//   createAdmin,
//   deleteAdmin,
//   readAllAdmin,
//   readSpecificAdmin,
//   updateAdmin,
// } from "../controller/adminController.js";

import { Router } from "express";
import { createAdmin, deleteAdmin, readAllAdmin, readSpecificAdmin, updateAdmin } from "../controller/adminController.js";

let adminRouter = Router();
//localhost:8000/admin =>give all admin
//localhost:8000/admin?fullName

adminRouter
  .route("/") //localhost:8000/admin
  .post(createAdmin)
  .get(readAllAdmin);

adminRouter
  .route("/:id") //localhost:8000/admin/any
  .get(readSpecificAdmin)
  .patch(updateAdmin)
  .delete(deleteAdmin);

export default adminRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create admin
get all admin

get specific admin


Admin.create(req.body)
Admin.find({})
Admin.findById(req.params.id)
Admin.findByIdAndUpdate(req.params.id,req.body)
Admin.findByIdAndDelete(req.params.id)

*/
