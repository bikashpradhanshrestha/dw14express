import { Router } from "express";
import {
  createReview,
  deleteReview,
  readAllReview,
  readSpecificReview,
  updateReview,
} from "../controller/reviewController.js";

let reviewRouter = Router();

reviewRouter
  .route("/") //localhost:8000/review
  .post(createReview)
  .get(readAllReview);

reviewRouter
  .route("/:id") //localhost:8000/review/any
  .get(readSpecificReview)
  .patch(updateReview)
  .delete(deleteReview);

export default reviewRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create review
get all review

get specific review


Review.create(req.body)
Review.find({})
Review.findById(req.params.id)
Review.findByIdAndUpdate(req.params.id,req.body)
Review.findByIdAndDelete(req.params.id)

*/
