import { Router } from "express";
import { handleMultipleFile, handleSingleFile } from "../controller/fileController.js";
import upload from "../utils/upload.js";


let fileRouter = Router();
//localhost:8000/file =>give all file
//localhost:8000/file?fullName

fileRouter
  .route("/single") //localhost:8000/file
  .post(upload.single("docs"),handleSingleFile);

  fileRouter.route("/multiple").post(upload.array("docs"),handleMultipleFile);
  export default fileRouter;



//upload.single("docs")
//used to take from data
//it takes file from postman and adda to the public
//it add file information to req.file
//it add others information to req.body 

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create file
get all file

get specific file


File.create(req.body)
File.find({})
File.findById(req.params.id)
File.findByIdAndUpdate(req.params.id,req.body)
File.findByIdAndDelete(req.params.id)

*/
