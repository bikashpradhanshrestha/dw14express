import { Router } from "express";

let traineeRouter = Router();

traineeRouter
  .route("/") //localhost:8000/bike
  .post((req, res, next) => {
    console.log(req.body);
    res.json({
      success: true,
      message: "trainee post successfully.",
    });
  })
  .get((req, res, next) => {
    res.json("bike get");
  })
  .patch((req, res, next) => {
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });

export default traineeRouter;
/* 
url=localhost:8000/trainees,post at response {success:true, message:"trainees created successfully"}
			url=localhost:8000/trainees,get at response {success:true, message:"trainees read successfully"}
			url=localhost:8000/trainees,patch at response {success:true, message:"trainees updated successfully"}
			url=localhost:8000/trainees,delete at response {success:true, message:"trainees deleted successfully"}

*/

/* 
url=localhost:8000/product,post at response {success:true, message:"product created successfully"}
url=localhost:8000/product,get at response {success:true, message:"product read successfully"}
url=localhost:8000/product,patch at response {success:true, message:"product updated successfully"}
url=localhost:8000/product,delete at response {success:true, message:"trainees deleted successfully"}




*/
